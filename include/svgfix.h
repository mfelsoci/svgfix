#ifndef __SVGFIX_H
#define __SVGFIX_H

#include "common.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

/**
 * @brief Check whether @string is a valid numeric value, integer or decimal.
 *
 * If a non-digit character or a second decimal point is encountered, the
 * function stops checking and returns the position of the latter. This also
 * happens if @arg string begins or ends with a decimal point.
 *
 * @param string The string to check.
 * @return -1 if @arg string represents a valid numeric value, the index of the
 *         first unwanted character otherwise.
 */
ssize_t check_number(const char * string);

/**
 * @brief Fix the attributes of the SVG image in the file pointed by @arg image.
 *
 * For an SVG image to preserve its default dimensions when displayed on an HTML
 * page, the viewBox and the preserveAspectRatio attributes must be set to the
 * correct values.
 *
 * This core function ensures that the SVG image file pointed by @arg image
 * features these attributes and that they are correctly configured.
 *
 * @param image Path to the SVG image file to process.
 * @returns true if all goes well, false otherwise.
 */
bool fix(const char * image);

#endif
