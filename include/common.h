#ifndef __COMMON_H
#define __COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include "config.h"

/**
 * Should hold a help message string displayed anytime the 'help' option is
 * present.
 */
extern const char * help_message;

/**
 * Should hold a usage message string displayed anytime the application
 * encounters a syntax error on start-up. The string must contain an '%s'
 * placeholder. The latter shall be replaced by the application name before
 * being displayed.
 */
extern const char * usage_message;

/**
 * @brief Prints the error message @arg message on the stadard error output. If
 * @arg message is NULL, a generic error message is displayed.
 *
 * @param message Error message to display.
 * @returns void
 */ 
void error(const char * message);

#endif
