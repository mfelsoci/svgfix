# svgfix (archive)

**This project has moved to [https://gitlab.com/mfelsoci/svgfix](https://gitlab.com/mfelsoci/svgfix).**

Small utility program to add or fix the `viewBox` and `preserveAspectRatio` attributes of SVG images to ensure their initial dimensions defined in `width` and `height` are respected when displayed on an HTML page.

## Building and installing

The machine used to build **svgfix** must satisfy the following software dependencies:

- CMake,
- a C compiler (e. g. `gcc`),
- LibXml2 (headers and development files).

The first step is to generate the *Makefile*. From the root of the source directory, run the following commands:

```shell
mkdir build && cd build
cmake ..
```

Then, compile and install **svgfix** on the system.

```shell
make install
```

## Usage

**svgfix** is a command-line tool using a very straightforward syntax:

```
svgfix [OPTION] SVG-FILE...
```

For example, to fix the attributes of the SVG image `folder/test.svg`, run

```shell
svgfix folder/test.svg
```

Please note, that the above command shall **override** the original image file.

Finally, the program features three options.

| Options | Description |
|:-------:|-------------|
| -h | Show a help message and exit.|
| -q | Quiet execution. Do not print anything on the standard output. |
| -v | Show version information and exit. |

## License

See the [COPYING](COPYING) file for license details.