#include "svgfix.h"

ssize_t check_number(const char * string) {
  bool decimal = false;
  for(ssize_t i = 0; string[i] != '\0'; i++) {
    if(string[i] == '.') {
      if(decimal) {
        return i;
      }

      if(string[i + 1] != '\0' && i > 0) {
        decimal = true;
      }
    } else if(string[i] < '0' || string[i] > '9') {
      return i;
    }
  }

  return -1;
}

bool fix(const char * image) {
  if(!image) {
    error("Invalid path to image!");
    return false;
  }

  if(strnlen(image, PATH_MAX) >= PATH_MAX) {
    error("Image file name is too long!");
    return EXIT_FAILURE;
  }

  xmlDocPtr svg = xmlParseFile(image);

  if(!svg) {
    error("Failed to parse the SVG image file!");
    return false;
  }

  xmlNodePtr root = xmlDocGetRootElement(svg);

  if(!root) {
    error("Failed to get the root the SVG image!");
    xmlFreeDoc(svg);
    return false;
  }

  bool success = true;

  xmlChar * width = xmlGetProp(root, "width"),
      * height = xmlGetProp(root, "height"),
      * view_box = xmlGetProp(root, "viewBox"),
      * preserve_aspect_ratio = xmlGetProp(root, "preserveAspectRatio");

  if(!width) {
    error("Failed to read the width of the image!");
    success = false;
    goto clean_init;
  }

  if(!height) {
    error("Failed to read the height of the image!");
    success = false;
    goto clean_init;
  }

  ssize_t number_end = check_number((char *) width);
  if(!number_end) {
    error("Invalid image width value!");
    goto clean_init;
  }

  if(number_end > 0) {
    width[number_end] = '\0';
  }

  number_end = check_number((char *) height);
  if(!number_end) {
    error("Invalid image height value!");
    goto clean_init;
  }

  if(number_end > 0) {
    height[number_end] = '\0';
  }

  size_t width_length = strlen(width), height_length = strlen(height);
  size_t attribute_length = width_length + height_length + 6;

  char * view_box_attribute = (char *) malloc(attribute_length * sizeof(char));
  if(!view_box_attribute) {
    error("Failed to allocate the viewBox attribute!");
    success = false;
    goto clean;
  }

  view_box_attribute = strncpy(view_box_attribute, "0 0 ", 5);
  if(!view_box_attribute) {
    error("Failed to construct the preserveAspectRatio attribute string!");
    free(view_box_attribute);
    success = false;
    goto clean;
  }

  view_box_attribute = strncat(view_box_attribute, width, width_length + 1);
  if(!view_box_attribute) {
    error("Failed to construct the preserveAspectRatio attribute string!");
    free(view_box_attribute);
    success = false;
    goto clean;
  }

  view_box_attribute = strncat(view_box_attribute, " ", 2);
  if(!view_box_attribute) {
    error("Failed to construct the preserveAspectRatio attribute string!");
    success = false;
    goto clean;
  }

  view_box_attribute = strncat(view_box_attribute, height, height_length + 1);
  if(!view_box_attribute) {
    error("Failed to construct the preserveAspectRatio attribute string!");
    success = false;
    goto clean;
  }

  if(view_box) {
    if(!xmlSetProp(root, "viewBox", view_box_attribute)) {
      error("Failed to update the value of the viewBox attribute!");
      success = false;
      goto clean;
    }
  } else {
    if(!xmlNewProp(root, "viewBox", view_box_attribute)) {
      error("Failed to add the viewBox attribute!");
      success = false;
      goto clean;
    }
  }

  if(preserve_aspect_ratio) {
    if(!xmlSetProp(root, "preserveAspectRatio", "xMinYMin meet")) {
      error("Failed to update the value of the preserveAspectRatio attribute!");
      success = false;
      goto clean;
    }
  } else {
    if(!xmlNewProp(root, "preserveAspectRatio", "xMinYMin meet")) {
      error("Failed to add the preserveAspectRatio attribute!");
      success = false;
      goto clean;
    }
  }

  if(xmlSaveFormatFile(image, svg, 1) < 1) {
    error("Error writing the output image file!");
    success = false;
    goto clean;
  }

clean:
  if(view_box_attribute) {
    free(view_box_attribute);
  }
clean_init:
  if(width) {
    free(width);
  }
  if(height) {
    free(height);
  }
  if(view_box) {
    free(view_box);
  }
  if(preserve_aspect_ratio) {
    free(preserve_aspect_ratio);
  }
  if(svg) {
    xmlFreeDoc(svg);
  }

  return success;
}
