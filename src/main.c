#include "svgfix.h"

const char * help_message =
    "Fix the attributes of SVG images to ensure their initial dimensions are "
    "respected when displayed on an HTML page.\n"
    "Usage: %s [OPTION] SVG-FILE...\n\n"
    "Options:\n"
    "  -h           Show this help message and exit.\n"
    "  -q           Quiet. Do not print anything on the standard output.\n"
    "  -v           Show version information and exit.\n\n"
    "Copyright (C) 2021  Inria\n"
    "Developed by Marek Felšöci.\n"
    "This program comes with ABSOLUTELY NO WARRANTY.\n"
    "This is free software, and you are welcome to redistribute it under "
    "the terms of the GNU General Public License.\n"
    "For more information about these matters, see the file named COPYING.\n";

const char * usage_message =
    "Arguments mismatch!\n"
    "Usage: %s [OPTION] SVG-FILE...\n\n"
    "Rerun using the '-h' option to get help.\n";

/**
 * The main program calls @fn fix on the SVG image file(s) passed as
 * argument(s).
 *
 * @param argc Total count of arguments, including the executable name.
 * @param argv List of arguments.
 * @return EXIT_FAILURE on failure, EXIT_SUCCESS otherwise.
 */
int main(int argc, char ** argv) {
  if(argc < 2) {
    fprintf(stderr, usage_message, argv[0]);
    return EXIT_FAILURE;
  }

  int opt;
  bool quiet = false;

  while((opt = getopt(argc, argv, "hqv")) != EOF) {
    switch(opt) {
      case 'h':
        printf(help_message, argv[0]);
        return EXIT_SUCCESS;
      case 'q':
        quiet = true;
        break;
      case 'v':
        printf("svgfix %d.%d\n", SVGFIX_VERSION_MAJOR, SVGFIX_VERSION_MINOR);
        return EXIT_SUCCESS;
      default:
        fprintf(stderr, usage_message, argv[0]);
        return EXIT_FAILURE;
    }
  }

  size_t file_count = (size_t) (argc - optind);
  if(file_count < 1) {
    fprintf(stderr, "No files to process.\n");
    return EXIT_SUCCESS;
  }

  for(size_t i = (size_t) optind; i < file_count + ((size_t) optind); i++) {
    if(!fix(argv[i])) {
      fprintf(stderr, "Processing of %s failed! Terminating...\n", argv[i]);
      return EXIT_FAILURE;
    } else if(!quiet) {
      printf("%s\n", argv[i]);
    }
  }

  printf("%lu file(s) processed.\n", file_count);
  return EXIT_SUCCESS;
}
