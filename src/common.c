#include "common.h"

void error(const char * message) {
  if(message) {
    if(errno) {
      perror(message);
    } else {
      fprintf(stderr, "%s", message);
    }

    return;
  }

  fprintf(stderr, "An unknown error occurred!");
  
  return;
}
